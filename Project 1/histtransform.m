%Point and histogram transform Project
%Author: Papas Ioannis 9218

function Y=histtransform(X,h,v)
% Y=histtransform(X,h,v) transforms a grayscale image based on given
% percentages with a greedy algorithm which are given manually or calculated from the pdf2hist
% function from a given pdf. So for example when we want the image histogram to
% approximate a uniform function, we use the pdf2hist with the function
% handle to be equal as unifpdf. We find the first h(1) percent of the
% amount of the pixels (from low to high) and we give them the value h(1). 
% Then h(2) and give them v(2) and so on. If the value at the index ,from
% which the previous part is to be given a value v(i), is equal with other
% values, we have to check that points with same numeric values in the 
% initial image will have same numeric values at the output image
% INPUTS:
% X: MxN matrix with the numeric values of the lighting of every pixel of
%    the initial image
% h: Kx1 vector of the percentages that will divide at parts all the points
% v: Kx1 vector of the color numeric values that will be given for each
%    part
% OUTPUTS:
% Y: MxN matrix with the numeric values of the lighting of every pixel of
%    the output image

N=length(X(1,:));
M=length(X(:,1));
L=M*N; %Number of pixels
tmp=X(:); %Remake the image to a vector
[tmp1,I]=sort(tmp); %Sort from low to high the numeric values while saving the initial positions
K=length(h);
h=floor(h*L); %Transform the percentages to amount of points
for iK=1:(K)
    %Check if one part will have points. If the percentage given is very low
    %and after the multiplication with the total number of points, the
    %points are zero, there is no reason to divide a part for 0 zero points
    %so we continue to the next index
    if(h(iK)==0)
        continue;
    end
    %Find until which point will we arrive at the sorted image vector
    end1=h(iK);
    %if we have transformed all the points then break the loop, because
    %there are no more points left to transform
    if isempty(I)==1
        break;
    end
    %If the point till which we will arrive is bigger than the left length
    %then change it so it arrives till the end of the left vector. For
    %example if there are left 10 points to transform, but the percentage
    %(part) says we need to transform 100 points, it will give an out of
    %bounds error. So we equalize our index with the left points (=10 in
    %the example)
    if end1>length(I)
        end1=length(I);
    end
    %After indexing the final point of a part to be transformed (at the sorted vector), we find
    %all the points that have the same numeric value as it, and we transfer
    %the index to the last one. This is because we want same values of the
    %initial photo to have same transformed values at the output
    end1=find(tmp1==tmp1(end1),1,'last');
    %Find all the indexes that will be transformed at each loop
    tmp2=I(1:end1);
    %Transform the points at those indexes with the color value given(at
    %the non sorted vector)
    tmp(tmp2)=v(iK);
    %Remove the indexes and the points from the sorted vector that were
    %transformed
    I(1:end1)=[];
    tmp1(1:end1)=[];
end
%Reshape the non sorted vector with the transformed values to its initial
%size/form and this is the output image
Y=reshape(tmp, [M N]);

end