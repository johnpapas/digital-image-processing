%Point and histogram transform Project
%Author: Papas Ioannis 9218

function Y=pointtransform(X,x,y)
% Y=pointtransform(X,x,y) transforms a grayscale image based on the points
% given. Each pair of consecutive points is a line based on which will the
% image be transformed
% INPUTS:
% X: MxN matrix with the numeric values of the lighting of every pixel of
%    the initial image
% x: Kx1 vector of the x-coordinates of the points, each one indicates the
%    numeric value of the initial image
% y: Kx1 vector of the y-coordinates of the points, each one indicates the
%    numeric value of the output image
% OUTPUTS:
% Y: MxN matrix with the numeric values of the lighting of every pixel of
%    the output image

Y=X; %Initialize the output image from the beginning, and make changes afterwards

%We need the 0 and 1 points so we can calculate every slope and every line
%equation afterwards
x=[0 x 1]; 
y=[0 y 1];
K=length(x);
b=(y(2:K)-y(1:K-1))./(x(2:K)-x(1:K-1)); %calculate the slope of every line
%for all the pair of points
for iK=1:K-1
    %Find which pixels have lightning between xi and xi+1. These are the points to be transformed
    S=(X>=x(iK))&(X<=x(iK+1));
    %The points are transformed based on the line equation y=b(x-x0)+y0
    Y(S)=b(iK)*(X(S)-x(iK))+y(iK);
end

end