%Point and histogram transform Project
%Author: Papas Ioannis 9218

%%
%Initial photo
X = imread('lena.bmp');
X = rgb2gray(X);
X = double(X) / 255;

[hn , hx] = hist(X(:), 0:1/255:1);
figure(1);
clf
bar(hx , hn)
%%
%Point Transform case 1, contrast stretching
x=[0.1961 0.8039];
y=[0.0392 0.9608];
Y=pointtransform(X,x,y);
figure(2);
clf
imshow(Y);

[hn , hx] = hist(Y(:), 0:1/255:1);
figure(3);
clf
bar(hx , hn)

%%
%Point Transform case 2, clipping
x=[0.5 0.5];
y=[0 1];
Y=pointtransform(X,x,y);
figure(4);
clf
imshow(Y);

[hn , hx] = hist(Y(:), 0:1/255:1);
figure(5);
clf
bar(hx , hn)

%%
%Hist transform case 1
L = 10;
v = linspace (0, 1, L);
h = ones([1, L]) / L;
Y=histtransform(X,h,v);

figure(6);
clf
imshow(Y);

[hn , hx] = hist(Y(:), 0:1/255:1);
figure(7);
clf
bar(hx , hn)

%%
%Hist transform case 2
L = 20;
v = linspace (0, 1, L);
h = ones([1, L]) / L;
Y=histtransform(X,h,v);

figure(8);
clf
imshow(Y);

[hn , hx] = hist(Y(:), 0:1/255:1);
figure(9);
clf
bar(hx , hn)

%%
%Hist transform case 3
L = 10;
v = linspace (0, 1, L);
h = normpdf(v, 0.5) / sum(normpdf(v, 0.5));
Y=histtransform(X,h,v);

figure(10);
clf
imshow(Y);

[hn , hx] = hist(Y(:), 0:1/255:1);
figure(11);
clf
bar(hx , hn)

%%
%Histogram based on pdf case 1 
d=0:0.2:1;
[h,v]=pdf2hist(d,@(x)unifpdf(x,0,1));
Y=histtransform(X,h,v);

figure(10);
clf
imshow(Y);

[hn , hx] = hist(Y(:), 0:1/255:1);
figure(11);
clf
bar(hx , hn)

%%
%Histogram based on pdf case 2 
d=0:0.4:2;
[h,v]=pdf2hist(d,@(x)unifpdf(x,0,2));
Y=histtransform(X,h,v);

figure(12);
clf
imshow(Y);

[hn , hx] = hist(Y(:), 0:1/255:2);
figure(13);
clf
bar(hx , hn)

%%
%Histogram based on pdf case 3 
d=0:0.1:1;
[h,v]=pdf2hist(d,@(x)normpdf(x,0.5,0.1));
Y=histtransform(X,h,v);

figure(14);
clf
imshow(Y);

[hn , hx] = hist(Y(:), 0:1/255:1);
figure(15);
clf
bar(hx , hn)