%Point and histogram transform Project
%Author: Papas Ioannis 9218

function [h,v]=pdf2hist(d,f)
% [h,v]=pdf2hist(d,f) calculates the probability or the percentage of the
% points that will take each of the colors v based on a given probability
% density function f. I.E. It creates a histogram from the given pdf used 
% to transform the image histogram to a new histogram that approximates the
% histogram of the given pdf. Also it calculates the color given to each 
% part of the pixels after they are divided
% INPUTS:
% d: Nx1 vector that each consecutive pair indicates a span [di di+1]
% f: function handle of the pdf. For example @(x)unifpdf(x,0,1) is used
%    for the new histogram to approximate the histogram of a uniform pdf
%    from 0 to 1
% OUTPUTS:
% h: (N-1)x1 vector which contains each percentage of the points that will
%    have an certain color
% v: (N-1)x1 vector which contains the colors for each part of the points
%    calculated as the middle of each [di di+1] span


N=length(d);
h=zeros(N-1,1);
v=zeros(N-1,1);

for i=1:N-1
%   h(i)=integral(f,d(i),d(i+1));
    %Calculate the probability for the a [di di+1] span from the pdf,
    %calculating the integral from di to di+1 of the function with the
    %numeric method of Simpson
    h(i)=((d(i+1)-d(i))/3)*(f(d(i))+f(d(i+1))+4*f((d(i+1)+d(i))/2));
    %Calculate the color given to each part of the pixels after they are
    %divided. The color is equal with the middle of the span.
    v(i)=(d(i+1)+d(i))/2;
end
%Normalize the histogram values so the sum of the percentages is 1
h=h/sum(h);



end